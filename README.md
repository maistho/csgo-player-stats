# CS:GO stats

## Getting started

First you will need to create a config file. The default name is `config.json`, but you can also specify which file to use with the CLI flag `--config`

Example config.json

```json
{
  "profileLink": "https://steamcommunity.com/id/Maistho",
  "steamToken": "123456789%7C%7C0028493FF02849BD99178ACBA1192"
}
```

The steam token can be found by opening dev tools when logged in at steamcommunity.com and in the Application tab under Cookies.

Copy the value of the cookie `steamLoginSecure`

**NOTE: I can technically do lots of bad stuff with this token. Don't use this tool unless you trust that the code isn't malicious**

## Usage

```
# Display help
npx csgo-player-stats help

# Fetch match data
npx csgo-player-stats fetch

# Display statistics
npx csgo-player-stats stats
```
