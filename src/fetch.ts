import axios from 'axios'
import { JSDOM } from 'jsdom'
import { Map, Match, Team } from './types'

export async function getAllMatches(config: {
  profileLink: string
  steamToken: string
}) {
  const matches: Match[] = []
  let continueToken: string | undefined
  do {
    const { data } = await axios.get(`${config.profileLink}/gcpd/730`, {
      headers: {
        Cookie: `steamLoginSecure=${config.steamToken};`,
      },
      params: {
        ajax: 1,
        tab: 'matchhistorycompetitive',
        continue_token: continueToken,
      },
    })
    if (!data.success) {
      throw new Error(JSON.stringify(data))
    }
    const dom = new JSDOM(data.html)
    matches.push(
      ...Array.from(
        dom.window.document.querySelectorAll(
          '.generic_kv_table.csgo_scoreboard_root>tbody>tr',
        ),
      )
        .map(tr => {
          return {
            map: getMap(tr),
            time: getTime(tr),
            waitTime: getWaitTime(tr),
            matchDuration: getMatchDuration(tr),
            score: getScore(tr),
            demo: getDemo(tr),
            ...getTeams(tr, config.profileLink),
          }
        })
        .filter(({ map }) => !!map),
    )
    continueToken = data.continue_token
  } while (continueToken)
  return matches
}

const getTeams = (tr: Element, profileLink: string) => {
  const teams: Team[] = [[], []]
  const players = Array.from(
    tr.querySelectorAll('.csgo_scoreboard_inner_right>tbody>tr'),
  )
  let currentTeam = -1
  for (const player of players) {
    const userLink = q(player, 'a.linkTitle')
    if (userLink == null) {
      currentTeam += 1
      continue
    }
    teams[currentTeam].push({
      link: userLink.attributes.getNamedItem('href')!.textContent || '',
      name: getInfo(player, 'a.linkTitle'),
      image: getAttribute(q(player, '.playerAvatar img'), 'src'),
      ping: parseInt(getInfo(player, 'td:nth-child(2)')),
      kills: parseInt(getInfo(player, 'td:nth-child(3)')),
      assists: parseInt(getInfo(player, 'td:nth-child(4)')),
      deaths: parseInt(getInfo(player, 'td:nth-child(5)')),
      mvps: parseInt(getInfo(player, 'td:nth-child(6)').substr(1)) || 0,
      headshotRate: getInfo(player, 'td:nth-child(7)'),
      score: parseInt(getInfo(player, 'td:nth-child(8)')),
    })
  }
  const userTeam = teams.findIndex(team =>
    team.some(player => player.link === profileLink),
  )
  return { teams, userTeam }
}

const getDemo = tr => {
  const text = q(tr, '.csgo_scoreboard_inner_left .csgo_scoreboard_btn_gotv')
  const button = text && text.parentElement
  return getAttribute(button, 'href')
}

const getScore = (tr: Element | null) => getInfo(tr, '.csgo_scoreboard_score')

const getMatchDuration = (tr: Element | null) =>
  getInfo(tr, '.csgo_scoreboard_inner_left>tbody>tr:nth-child(4)').replace(
    /^Match Duration: /,
    '',
  )

const getWaitTime = (tr: Element | null) =>
  getInfo(tr, '.csgo_scoreboard_inner_left>tbody>tr:nth-child(3)').replace(
    /^Wait Time: /,
    '',
  )
const getTime = (tr: Element | null) =>
  new Date(getInfo(tr, '.csgo_scoreboard_inner_left>tbody>tr:nth-child(2)'))
const getMap = (tr: Element | null) =>
  getInfo(tr, '.csgo_scoreboard_inner_left>tbody>tr:nth-child(1)').replace(
    /^Competitive /,
    '',
  ) as Map

function getInfo(el: Element | null, query: string): string {
  return getText(q(el, query))
}

function getText(el: Element | null): string {
  return ((el && el.textContent) || '').trim()
}

function q(el: Element | null, query: string): Element | null {
  return el && el.querySelector(query)
}

function getAttribute(el: Element | null, attribute: string) {
  return ((el && el.getAttribute(attribute)) || '').trim()
}
