import { Match } from './types'

type MapData = {
  [key: string]: {
    name: string
    timesPlayed: number
    wins: number
    losses: number
    draws: number
    winrate: number
  }
}
export function parseMatches(data: Match[]) {
  const maps: MapData = {}

  data.forEach((match: Match) => {
    const score = match.score.split(':').map(s => s.trim())
    const myTeam = match.userTeam
    const notMyTeam = myTeam === 0 ? 1 : 0
    if (!maps[match.map]) {
      maps[match.map] = {
        name: match.map,
        timesPlayed: 0,
        wins: 0,
        losses: 0,
        draws: 0,
        winrate: 0,
      }
    }
    const map = maps[match.map]
    if (score[myTeam] > score[notMyTeam]) {
      map.wins += 1
    } else if (score[myTeam] < score[notMyTeam]) {
      map.losses += 1
    } else {
      map.draws += 1
    }
    map.timesPlayed += 1
  })

  Object.keys(maps).forEach(map => {
    maps[map].winrate =
      maps[map].wins / (maps[map].timesPlayed - maps[map].draws)
  })
  return maps
}

export function printStatistics(maps: MapData) {
  const longestMapName = Math.max(...Object.keys(maps).map(map => map.length))
  console.log(
    `${'Map'.padEnd(longestMapName + 1, ' ')} ${'Winrate'.padEnd(
      7,
      ' ',
    )} (W/D/L)`,
  )
  Object.values(maps)
    .sort((a, b) => b.winrate - a.winrate)
    .forEach(map => {
      console.log(
        `${`${map.name}:`.padEnd(longestMapName + 1, ' ')} ${formatWinrate(
          map.winrate,
        ).padEnd(7, ' ')} (${map.wins}/${map.draws}/${map.losses})`,
      )
    })
}

function formatWinrate(winrate) {
  return (
    (winrate * 100).toLocaleString(undefined, {
      maximumFractionDigits: 2,
      minimumFractionDigits: 0,
    }) + '%'
  )
}
