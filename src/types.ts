export enum Map {
  de_mirage = 'Mirage',
  de_dust2 = 'Dust II',
  de_subzero = 'Subzero',
  de_train = 'Train',
  de_nuke = 'Nuke',
  de_inferno = 'Inferno',
  de_cache = 'Cache',
  de_overpass = 'Overpass',
}

export interface Match {
  map: Map
  time: Date
  waitTime: string
  matchDuration: string
  score: string
  demo?: string
  teams: Team[]
  userTeam: number
}
export type Team = Player[]

export interface Player {
  link: string
  image: string
  name: string
  ping: number
  kills: number
  assists: number
  deaths: number
  mvps: number
  headshotRate: string
  score: number
}
