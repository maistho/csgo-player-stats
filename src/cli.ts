#!/usr/bin/env node

import yargs = require('yargs')
import ora = require('ora')

import { getAllMatches } from './fetch'
import { writeFileSync, existsSync } from 'fs'
import { resolve } from 'path'
import { parseMatches, printStatistics } from './stats'

yargs
  .command(
    'fetch [export]',
    'fetch all matches',
    args =>
      args.positional('export', {
        describe: 'File to export matches to',
        default: 'export.json',
      }),
    argv => {
      const exportLocation = resolve(argv.export as string)
      if (existsSync(exportLocation)) {
        handleError(`${exportLocation} already exists, not overwriting`)
      }
      const configLocation = resolve(argv.config as string)
      const config = require(configLocation)
      const spinner = ora('Fetching matches').start()

      getAllMatches(config).then(
        matches => {
          spinner.succeed()
          writeFileSync(exportLocation, JSON.stringify(matches, undefined, 2))
        },
        err => {
          spinner.fail()
          handleError(err)
        },
      )
    },
  )
  .command({
    command: 'stats [file]',
    describe: 'print stats from file',
    builder: args =>
      args.positional('file', {
        describe: 'File to import statistics from',
        default: 'export.json',
      }),
    handler: argv => {
      const data = require(resolve(argv.file as string))
      const stats = parseMatches(data)
      printStatistics(stats)
    },
  })
  .option('config', {
    alias: 'c',
    default: 'config.json',
  })
  .demandCommand()
  .help().argv

function handleError(err) {
  const scriptName =
    process.argv[0] === 'node' ? process.argv[1] : process.argv[0]
  console.warn(`${scriptName} encountered an error:\n\t`, err, '\n')
  process.exit(1)
}
